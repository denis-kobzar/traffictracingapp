﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TrafficTracingApp.Models;

namespace TrafficTracingApp.CommandTools
{
    class CmdTool
    {
        public static async Task<IpRouteModel> GetTracingIpList(string endIp)
        {
            try
            {
                IpRouteModel ipList = new IpRouteModel();

                var info = await StartCommandInCmdOrException($"tracert {endIp}");

                Regex rx = new Regex(@"(\d+\.\d+\.\d+\.\d+)", RegexOptions.Multiline);
                MatchCollection ipMatchCollection = rx.Matches(info);

                foreach (Match match in ipMatchCollection)
                {
                    ipList.Add(new IpAddressModel(match.Groups[0].Value.Trim()));
                }

                return ipList;
            }
            catch (InvalidOperationException ex)
            {
                throw ex;
            }
        }

        public static async Task<string> StartCommandInCmdOrException(string command)
        {
            try
            {
                Process cmd = null;
                var startInfo = new ProcessStartInfo
                {
                    FileName = "cmd.exe",
                    Arguments = $"/c{command}",
                    RedirectStandardOutput = true
                };
                
                cmd = Process.Start(startInfo);
                StreamReader streamReader = cmd.StandardOutput;
                var info = await streamReader.ReadToEndAsync();

                return info;
            }
            catch (InvalidOperationException ex)
            {
                throw ex;
            }
        }
    }
}
