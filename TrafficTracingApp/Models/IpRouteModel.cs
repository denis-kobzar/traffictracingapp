﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrafficTracingApp.Models
{
    class IpRouteModel
    {
        public readonly List<IpAddressModel> Route = new List<IpAddressModel>();

        public void Add(IpAddressModel ipAddress)
        {
            Route.Add(ipAddress);
        }

        public bool Remove(IpAddressModel ipAddress)
        {
            return Route.Remove(ipAddress);
        }

        public void RemoveAt(int index)
        {
            Route.RemoveAt(index);
        }
    }
}
