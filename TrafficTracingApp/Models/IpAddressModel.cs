﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrafficTracingApp.Models
{
    class IpAddressModel
    {
        public string Ip { get; set; }
        public string CountryCode { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public int ZipCode { get; set; }
        public string ProviderName { get; set; }
        public string ProviterWebSite { get; set; }
        public string Host { get; set; }

        public IpAddressModel(string ip)
        {
            Ip = ip;
        }

        public string ToString() => Ip;
    }
}
