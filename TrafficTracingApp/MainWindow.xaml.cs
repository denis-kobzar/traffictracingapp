﻿using System;
using System.Windows;
using TrafficTracingApp.CommandTools;

namespace TrafficTracingApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private async void executeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ipListbox.Items.Clear();
                ipListbox.Items.Add("Diagnostics...");
                var ipList = await CmdTool.GetTracingIpList("8.8.8.8");
                ipListbox.Items.Clear();
                foreach (var item in ipList.Route)
                {
                    ipListbox.Items.Add(item.ToString());
                }
                MessageBox.Show("Diagnostics is Done!", "Info");
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }
    }
}
