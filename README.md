# TrafficTracingApp

## Overview

The main goal of the project is to enable the user to check whether his Internet traffic passes through unwanted areas. Unwanted areas include countries and cities through which traffic should not pass. The program checks all points and warns if traffic falls into unwanted zones.

## About commits
The names of the commits were taken from the names of issues in the gitlab, so for more information on any commit, you should find it by name in issues